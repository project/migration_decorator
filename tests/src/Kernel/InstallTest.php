<?php

namespace Drupal\Tests\migration_decorator\Kernel;

use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migration_decorator\MigrationDiscoveryDecoratorManagerInterface;
use Drupal\migration_decorator\Plugin\migration_decorator\Decorator\Fallback;

/**
 * Tests module install.
 *
 * @group migration_decorator
 */
class InstallTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'tecla',
  ];

  /**
   * Tests module installation.
   */
  public function testInstall(): void {
    $module_installer = \Drupal::service('module_installer');
    assert($module_installer instanceof ModuleInstallerInterface);
    $this->assertTrue($module_installer->install(['migration_decorator']));

    $discovery_decorator = \Drupal::service('plugin.manager.migration_discovery_decorator');
    assert($discovery_decorator instanceof MigrationDiscoveryDecoratorManagerInterface);
    $this->assertNull($discovery_decorator->getActiveInstance());

    $migration_manager = \Drupal::service('plugin.manager.migration');
    assert($migration_manager instanceof MigrationPluginManagerInterface);
    $this->assertIsArray($migration_manager->getDefinitions());

    $module_installer->install(['migmag']);
    \Drupal::service('kernel')->rebuildContainer();
    $discovery_decorator = \Drupal::service('plugin.manager.migration_discovery_decorator');

    $this->assertInstanceOf(Fallback::class, $discovery_decorator->getActiveInstance());
  }

}
