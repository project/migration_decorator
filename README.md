INTRODUCTION
------------

Migration Decorator provides a plugin which defines how the discovered migration
definitions should be decorated before they are passed to alter hooks.
With your own decorators, you can 
  - modify definitions you want to modify
  - perform an extra derivative calculations even on already-derived migrations
  - entirely remove certain definitions.


REQUIREMENTS
------------

No extra requirements.


INSTALLATION
------------

You can install Migration Decorator as you would normally install a contributed
Drupal module.


CONFIGURATION
-------------

This module does not have any configuration option.


USAGE
-----

There is only one "winner" decorator plugin instance which gets the chance to
decorate migration plugins: the plugin with the lowest weight always wins.
You can check
`\Drupal\migration_decorator\Plugin\migration_decorator\Decorator\Fallback` for
an example.


MAINTAINERS
-----------

* Zoltán Horváth (huzooka) - https://www.drupal.org/u/huzooka

This project has been sponsored by [Kibit Solutions][1].

[1]: https://kibitsolutions.com/
