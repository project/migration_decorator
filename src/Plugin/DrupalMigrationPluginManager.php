<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator\Plugin;

use Drupal\migration_decorator\Traits\MigrationManagerDiscoveryTrait;
use Drupal\migrate_drupal\MigrationPluginManager as BaseDrupalMigrationPluginManager;

/**
 * Manager class override for Migrate Drupal's migration plugin manager.
 */
final class DrupalMigrationPluginManager extends BaseDrupalMigrationPluginManager {

  use MigrationManagerDiscoveryTrait;

}
