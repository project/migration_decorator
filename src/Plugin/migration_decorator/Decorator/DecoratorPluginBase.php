<?php

namespace Drupal\migration_decorator\Plugin\migration_decorator\Decorator;

use Drupal\Component\Plugin\PluginBase;
use Drupal\migration_decorator\MigrationDiscoveryDecoratorInterface;

/**
 * Base plugin for migration discovery decorators.
 */
class DecoratorPluginBase extends PluginBase implements MigrationDiscoveryDecoratorInterface {

  /**
   * {@inheritdoc}
   */
  public function getEarlyMigrationDiscoveryDecorators(): array {
    return $this->pluginDefinition['early_migration_discovery_decorators'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationDiscoveryDecorators(): array {
    return $this->pluginDefinition['migration_discovery_decorators'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->pluginDefinition['weight'];
  }

}
