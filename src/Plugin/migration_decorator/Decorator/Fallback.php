<?php

namespace Drupal\migration_decorator\Plugin\migration_decorator\Decorator;

use Drupal\migration_decorator\Plugin\migrate\Discovery\ContainerDerivativeDiscoveryWithAutoDeriverDecorator;
use Drupal\migration_decorator\Plugin\migrate\Discovery\FilterDecorator;

/**
 * A migration discovery decorator plugin example (and fallback).
 *
 * @MigrationDiscoveryDecorator(
 *   id = "fallback",
 *   weight = 100,
 *   provider = "migmag",
 * )
 */
class Fallback extends DecoratorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getMigrationDiscoveryDecorators(): array {
    return [
      FilterDecorator::class,
      ContainerDerivativeDiscoveryWithAutoDeriverDecorator::class,
    ];
  }

}
