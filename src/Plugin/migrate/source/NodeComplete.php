<?php

namespace Drupal\migration_decorator\Plugin\migrate\source;

use Drupal\migration_decorator\Plugin\migrate\Traits\ChoppedSourceTrait;
use Drupal\node\Plugin\migrate\source\d7\NodeComplete as BaseNodeComplete;

/**
 * Autoderiver compatible node source plugin.
 *
 * @MigrateSource(
 *   id = "ad_d7_node_complete",
 *   source_module = "node"
 * )
 */
class NodeComplete extends BaseNodeComplete {

  use ChoppedSourceTrait;

}
