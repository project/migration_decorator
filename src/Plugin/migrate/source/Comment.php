<?php

namespace Drupal\migration_decorator\Plugin\migrate\source;

use Drupal\comment\Plugin\migrate\source\d7\Comment as BaseComment;
use Drupal\migration_decorator\Plugin\migrate\Traits\ChoppedSourceTrait;

/**
 * Autoderiver compatible comment source plugin.
 *
 * @MigrateSource(
 *   id = "ad_d7_comment",
 *   source_module = "comment"
 * )
 */
class Comment extends BaseComment {

  use ChoppedSourceTrait;

}
