<?php

namespace Drupal\migration_decorator\Plugin\migrate\Traits;

/**
 * Helper trait for chopped migration's source plugin.
 */
trait ChoppedSourceTrait {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $this->addAutoDeriverConditions($query);
    return $query;
  }

  /**
   * Adds the appropriate autoderiver conditions to the query, if any.
   */
  protected function addAutoDeriverConditions($query): void {
    if (
      isset($this->configuration['auto_deriver']['property']) &&
      isset($this->configuration['auto_deriver']['min']) &&
      isset($this->configuration['auto_deriver']['max'])
    ) {
      $ad = $this->configuration['auto_deriver'];
      $query->condition($ad['property'], $ad['min'], '>=');
      $query->condition($ad['property'], $ad['max'], '<=');
    }
  }

}
