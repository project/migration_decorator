<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator\Plugin\migrate\Traits;

use Drupal\migmag\Utility\MigMagSourceUtility;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migration_decorator\Plugin\migrate\Derivative\AutoDeriver;

/**
 * Trait for splitting big migrations into smaller chunks.
 */
trait AutoDeriverTrait {

  /**
   * Splits the current migration into smaller derivatives.
   *
   * @param \Drupal\migrate\Plugin\migrate\source\SqlBase $source
   *   The SQL source plugin.
   * @param array $base_plugin_definition
   *   The plugin definition.
   * @param string $column
   *   Name of the column splitting bases on.
   *
   * @return array[]
   *   The derivatives.
   */
  protected static function buildDerivatives(SqlBase $source, array $base_plugin_definition, string $column): array {
    $limit = static::getSplitLimit($base_plugin_definition);

    if ($source->count() <= static::getSplitThreshold($base_plugin_definition)) {
      return [$base_plugin_definition];
    }

    $derivatives = [];
    $ad_initial_min = isset($base_plugin_definition['source']['auto_deriver']['initial_min'])
      ? (int) $base_plugin_definition['source']['auto_deriver']['initial_min']
      : NULL;
    $min = static::getMininumOfColumn($source, $column);
    if (isset($ad_initial_min)) {
      if ($ad_initial_min > $min) {
        throw new \LogicException(sprintf(
          "The defined initial minimum value of the autoderiver column '%s' in migration base plugin definition '%s' is higher than the discovered minimum of the column. Initial value set is %s while the discovered minimum is %s.",
          $column,
          $base_plugin_definition['id'],
          $ad_initial_min,
          $min
        ));
      }

      $min = $ad_initial_min;
    }
    $max = static::getMaximumOfColumn($source, $column);
    $autoderiver_source_plugin_id = static::getSourcePluginId($base_plugin_definition);
    $source_id_alias = $source->getIds()[$column]['alias'] ?? NULL;
    $aliased_property = implode(
      '.',
      array_filter([$source_id_alias, $column])
    );
    // Derivatives must be predictable in order for being able to use highwater
    // property or just re-executing migrations (which pick up new items
    // automatically). So we always start with a predictable number.
    $start = (int) (floor($min / $limit) * $limit);
    while ($start <= $max) {
      $ad_min = $start;
      $ad_max = $start + ($limit - 1);

      $derivative = $base_plugin_definition;
      $derivative['migration_tags'][] = AutoDeriver::TAG;
      $derivative['source']['plugin'] = $autoderiver_source_plugin_id;
      $derivative['source']['auto_deriver']['property'] = $aliased_property;
      $derivative['source']['auto_deriver']['min'] = $ad_min;
      $derivative['source']['auto_deriver']['max'] = $ad_max;
      $derivative_id = 'from' . static::getPadded($ad_min, $max) . 'to' . static::getPadded($ad_max, $max);

      $derivatives[$derivative_id] = $derivative;
      $start += $limit;
    }

    static::filterDerivatives($derivatives);

    return $derivatives;
  }

  /**
   * Filters the calculated derivatives.
   *
   * @param array $derivatives
   *   Array of the calculated migration plugin definitions.
   */
  protected static function filterDerivatives(array &$derivatives): void {
    foreach ($derivatives as $id => $derivative) {
      if (!MigMagSourceUtility::getSourcePlugin($derivative['source'])->count()) {
        unset($derivatives[$id]);
      }
    }
  }

  /**
   * Builds a padded string from the given number.
   *
   * @param string|int $number
   *   The number to convert a padded string.
   * @param string|int $max
   *   The maximal number - this is used to determine the ideal length of the
   *   padded string.
   *
   * @return string
   *   The padded string.
   */
  protected static function getPadded($number, $max): string {
    $length_of_max_as_string = strlen((string) $max);
    return str_pad(
      (string) $number,
      $length_of_max_as_string,
      '0',
      STR_PAD_LEFT
    );
  }

  /**
   * Returns the source's lowest value of the specified column.
   *
   * @param \Drupal\migrate\Plugin\migrate\source\SqlBase $source
   *   A SQL source plugin.
   * @param string $property
   *   The name of the column.
   *
   * @return string
   *   The lowest value of the column.
   */
  protected static function getMininumOfColumn(SqlBase $source, string $property): string {
    return $source->getDatabase()->select($source->query(), 'min')
      ->fields('min', [$property])
      ->orderBy("min.$property", 'ASC')
      ->range(0, 1)
      ->execute()
      ->fetchField();
  }

  /**
   * Returns the source's highest value of the specified column.
   *
   * @param \Drupal\migrate\Plugin\migrate\source\SqlBase $source
   *   A SQL source plugin.
   * @param string $property
   *   The name of the column.
   *
   * @return string
   *   The highest value of the column.
   */
  protected static function getMaximumOfColumn(SqlBase $source, string $property): string {
    return $source->getDatabase()->select($source->query(), 'max')
      ->fields('max', [$property])
      ->orderBy("max.$property", 'DESC')
      ->range(0, 1)
      ->execute()
      ->fetchField();
  }

}
