<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator\Plugin\migrate\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\migmag\Utility\MigMagSourceUtility;
use Drupal\migrate\Plugin\migrate\source\SqlBase;

/**
 * Fallback deriver class for tagging big migrations.
 */
class AutoDeriver extends DeriverBase {

  /**
   * The default limit.
   *
   * @const int
   */
  const LIMIT = 10000;

  /**
   * The default split threshold.
   *
   * @const int
   */
  const SPLIT_THRESHOLD = 14000;

  /**
   * Tag added to chopped migration plugin definitions.
   *
   * @const string
   */
  const TAG = 'Auto Derived';

  /**
   * Tag to add to big migration plugin definitions which cannot be split.
   *
   * @const string
   */
  const TOO_BIG_TAG = 'Auto Deriver Too Big';

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $source = MigMagSourceUtility::getSourcePlugin($base_plugin_definition['source']);
    if (!$source instanceof SqlBase) {
      $this->derivatives = [$base_plugin_definition];
      return $this->derivatives;
    }

    $ref = new \ReflectionClass($source);
    $ref_iterator_init = $ref->getMethod('initializeIterator');
    $iterator_init_declarer_class = $ref_iterator_init->getDeclaringClass()->getName();

    if ($iterator_init_declarer_class !== SqlBase::class) {
      $this->derivatives = [$base_plugin_definition];
      return $this->derivatives;
    }

    try {
      $count = $source->count(TRUE);
      if ($count > self::SPLIT_THRESHOLD) {
        $base_plugin_definition['migration_tags'][] = self::TOO_BIG_TAG;
      }
      $this->derivatives = [$base_plugin_definition];
    }
    catch (\Exception $e) {
      $this->derivatives = [$base_plugin_definition];
    }

    return $this->derivatives;
  }

}
