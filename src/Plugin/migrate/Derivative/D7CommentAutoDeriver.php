<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator\Plugin\migrate\Derivative;

use Drupal\comment\Plugin\migrate\source\d7\Comment;
use Drupal\migmag\Utility\MigMagSourceUtility;

/**
 * Splits d7_comment migration into smaller chunks.
 */
class D7CommentAutoDeriver extends ChopperDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected static $splitThreshold = 14000;

  /**
   * {@inheritdoc}
   */
  protected static $limit = 10000;

  /**
   * {@inheritdoc}
   */
  public static function getSourcePluginId(array $base_plugin_definition): string {
    return 'ad_d7_comment';
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $source = MigMagSourceUtility::getSourcePlugin($base_plugin_definition['source']);
    assert($source instanceof Comment);

    $this->derivatives = static::buildDerivatives($source, $base_plugin_definition, 'cid');

    return $this->derivatives;
  }

}
