<?php

namespace Drupal\migration_decorator\Plugin\migrate\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\migration_decorator\Plugin\migrate\Traits\AutoDeriverTrait;

/**
 * Base class for chopper deriver classes of big migrations.
 */
abstract class ChopperDeriverBase extends DeriverBase implements ChopperDeriverInterface {

  use AutoDeriverTrait;

  /**
   * The limit.
   *
   * @var int
   */
  protected static $limit;

  /**
   * The split threshold.
   *
   * @var int
   */
  protected static $splitThreshold;

  /**
   * {@inheritdoc}
   */
  public static function getSplitLimit(array $base_plugin_definition): int {
    return static::$limit ?? AutoDeriver::LIMIT;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSplitThreshold(array $base_plugin_definition): int {
    return static::$splitThreshold ?? AutoDeriver::SPLIT_THRESHOLD;
  }

}
