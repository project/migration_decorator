<?php

namespace Drupal\migration_decorator\Plugin\migrate\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverInterface;

/**
 * Interface of migration chopper deriver classes.
 */
interface ChopperDeriverInterface extends DeriverInterface {

  /**
   * Returns the limit.
   *
   * @param array $base_plugin_definition
   *   The base plugin definition.
   *
   * @return int
   *   The limit.
   */
  public static function getSplitLimit(array $base_plugin_definition): int;

  /**
   * Returns the split threshold.
   *
   * @param array $base_plugin_definition
   *   The base plugin definition.
   *
   * @return int
   *   The split threshold.
   */
  public static function getSplitThreshold(array $base_plugin_definition): int;

  /**
   * Returns the plugin ID of the source plugin which is able to handle chunks.
   *
   * @param array $base_plugin_definition
   *   The base plugin definition.
   *
   * @return string
   *   The plugin ID of the source plugin which is able to handle chunks.
   */
  public static function getSourcePluginId(array $base_plugin_definition): string;

}
