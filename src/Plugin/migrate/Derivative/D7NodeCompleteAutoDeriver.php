<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator\Plugin\migrate\Derivative;

use Drupal\migmag\Utility\MigMagSourceUtility;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Splits d7_node_complete migrations into smaller chunks.
 */
class D7NodeCompleteAutoDeriver extends ChopperDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected static $splitThreshold = 7000;

  /**
   * {@inheritdoc}
   */
  protected static $limit = 5000;

  /**
   * {@inheritdoc}
   */
  public static function getSourcePluginId(array $base_plugin_definition): string {
    return 'ad_d7_node_complete';
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $source = MigMagSourceUtility::getSourcePlugin($base_plugin_definition['source']);
    assert($source instanceof DrupalSqlBase);

    $this->derivatives = static::buildDerivatives($source, $base_plugin_definition, 'nid');

    return $this->derivatives;
  }

}
