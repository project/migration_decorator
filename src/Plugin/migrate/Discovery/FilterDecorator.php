<?php

namespace Drupal\migration_decorator\Plugin\migrate\Discovery;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Component\Plugin\Discovery\DiscoveryTrait;

/**
 * Drops discovered definitions based on their base or full plugin IDs.
 *
 * This decorator is an example how one could filter out unnecessary migration
 * plugin definitions.
 */
class FilterDecorator implements DiscoveryInterface {

  use DiscoveryTrait;

  /**
   * Migration base plugin IDs to ignore.
   *
   * @const string[]
   */
  const IGNORED_BASE_PLUGIN_IDS = [
    'foo',
  ];

  /**
   * Migration plugin IDs to ignore.
   *
   * @const string[]
   */
  const IGNORED_FULL_PLUGIN_IDS = [
    'foo:bar:baz',
  ];

  /**
   * The Discovery object being decorated.
   *
   * @var \Drupal\Component\Plugin\Discovery\DiscoveryInterface
   */
  protected $decorated;

  /**
   * Constructs an EarlyFilterDecorator object.
   *
   * @param \Drupal\Component\Plugin\Discovery\DiscoveryInterface $decorated
   *   The object implementing DiscoveryInterface that is being decorated.
   */
  public function __construct(DiscoveryInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $unfiltered = $this->decorated->getDefinitions();
    $without_ignored_base_ids = array_filter(
      $unfiltered,
      function (array $definition) {
        return !in_array($definition['id'], static::IGNORED_BASE_PLUGIN_IDS);
      }
    );
    return array_filter(
      $without_ignored_base_ids,
      function (string $plugin_id) {
        return !in_array($plugin_id, static::IGNORED_FULL_PLUGIN_IDS);
      },
      ARRAY_FILTER_USE_KEY
    );
  }

  /**
   * Passes through all unknown calls onto the decorated object.
   *
   * @param string $method
   *   The method to call on the decorated object.
   * @param array $args
   *   Call arguments.
   *
   * @return mixed
   *   The return value from the method on the decorated object.
   */
  public function __call($method, array $args) {
    return call_user_func_array([$this->decorated, $method], $args);
  }

}
