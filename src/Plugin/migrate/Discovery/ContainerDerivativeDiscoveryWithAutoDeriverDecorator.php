<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator\Plugin\migrate\Discovery;

use Drupal\Component\Plugin\Derivative\DeriverInterface;
use Drupal\Component\Plugin\Exception\InvalidDeriverException;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\migration_decorator\Plugin\migrate\Derivative\AutoDeriver;

/**
 * Discovery for migration plugin definitions.
 *
 * This decorator is an example how users can add and evaluate additional
 * migration derivatives.
 */
class ContainerDerivativeDiscoveryWithAutoDeriverDecorator extends ContainerDerivativeDiscoveryDecorator {

  /**
   * {@inheritdoc}
   */
  protected function getDeriverClass($base_definition) {
    $base_id = $base_definition['id'];
    $camelcased = str_replace(
      ' ',
      '',
      ucwords(implode(
        ' ',
        array_filter(
          explode('_', $base_id)
        )
      ))
    );

    $auto_deriver_class = 'Drupal\\migration_decorator\\Plugin\\migrate\\Derivative\\' . $camelcased . 'AutoDeriver';
    $class = class_exists($auto_deriver_class, TRUE)
      ? $auto_deriver_class
      : AutoDeriver::class;

    if (!in_array(DeriverInterface::class, class_implements($class), TRUE)) {
      throw new InvalidDeriverException();
    }
    return $class;
  }

}
