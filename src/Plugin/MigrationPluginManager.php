<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator\Plugin;

use Drupal\migration_decorator\Traits\MigrationManagerDiscoveryTrait;
use Drupal\migrate\Plugin\MigrationPluginManager as BaseMigrationPluginManager;

/**
 * Manager class override for Migrate module's migration plugin manager.
 */
final class MigrationPluginManager extends BaseMigrationPluginManager {

  use MigrationManagerDiscoveryTrait;

}
