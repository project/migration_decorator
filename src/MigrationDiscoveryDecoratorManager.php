<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\migration_decorator\Annotation\MigrationDiscoveryDecorator;

/**
 * Manager of migration discovery decorator plugins.
 */
class MigrationDiscoveryDecoratorManager extends DefaultPluginManager implements MigrationDiscoveryDecoratorManagerInterface {

  /**
   * Construct a new MigrationDiscoveryDecoratorManager instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend for the definitions. We are using the same one what
   *   migration plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/migration_decorator/Decorator', $namespaces, $module_handler, MigrationDiscoveryDecoratorInterface::class, MigrationDiscoveryDecorator::class);
    $this->alterInfo('migration_discovery_decorator');
    $this->setCacheBackend($cache_backend, 'migration_discovery_decorator_plugins', ['migration_discovery_decorator_plugins']);
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveInstance(): ?MigrationDiscoveryDecoratorInterface {
    $winner = NULL;
    $definitions = $this->getDefinitions();
    usort($definitions, function ($a, $b) {
      return SortArray::sortByWeightElement($a, $b);
    });

    if (!empty($definitions)) {
      $winner = $this->createInstance(reset($definitions)['id']);
    }

    return $winner instanceof MigrationDiscoveryDecoratorInterface
      ? $winner
      : NULL;
  }

}
