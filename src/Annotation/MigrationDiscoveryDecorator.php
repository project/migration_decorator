<?php

namespace Drupal\migration_decorator\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a migration discovery decorator plugin annotation object.
 *
 * @see \Drupal\migration_decorator\MediaSourceInterface
 * @see \Drupal\migration_decorator\MediaSourceBase
 * @see \Drupal\migration_decorator\MediaSourceManager
 * @see hook_migration_discovery_decorator_info_alter()
 * @see plugin_api
 *
 * @Annotation
 */
class MigrationDiscoveryDecorator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * Migration discovery decorators to apply before derivatives are calculated.
   *
   * @var string[]|null
   */
  public $early_migration_discovery_decorators;

  /**
   * Additional migration discovery decorator classes.
   *
   * @var string[]|null
   */
  public $migration_discovery_decorators;

  /**
   * Weight of the plugin.
   *
   * @var int
   */
  public $weight = 0;

}
