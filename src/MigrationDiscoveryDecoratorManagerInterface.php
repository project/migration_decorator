<?php

namespace Drupal\migration_decorator;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface of migration discovery decorator managers.
 */
interface MigrationDiscoveryDecoratorManagerInterface extends PluginManagerInterface {

  /**
   * Returns the active decorator plugin instance.
   *
   * @return \Drupal\migration_decorator\MigrationDiscoveryDecoratorInterface|null
   *   The active decorator plugin instance.
   */
  public function getActiveInstance(): ?MigrationDiscoveryDecoratorInterface;

}
