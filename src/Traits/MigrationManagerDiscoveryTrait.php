<?php

declare(strict_types = 1);

namespace Drupal\migration_decorator\Traits;

use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\migration_decorator\MigrationDiscoveryDecoratorManagerInterface;

/**
 * Trait for migration plugin managers being made compatible with Aoutoderiver.
 */
trait MigrationManagerDiscoveryTrait {

  /**
   * The migration decorator manager, if any.
   *
   * @var \Drupal\migration_decorator\MigrationDiscoveryDecoratorManagerInterface|null
   */
  protected $decoratorManager;

  /**
   * Gets the plugin discovery.
   *
   * This method overrides DefaultPluginManager::getDiscovery() in order to
   * search for migration configurations in the MODULENAME/migrations
   * directory.
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $parent_discovery = parent::getDiscovery();
      if ($decorator = $this->decoratorManager->getActiveInstance()) {
        if ($early_decorators = $decorator->getEarlyMigrationDiscoveryDecorators()) {
          // Assume that the last decorator was
          // ContainerDerivativeDiscoveryDecorator.
          $discovery_ref = new \ReflectionClass($parent_discovery);
          $decorated_ref_prop = $discovery_ref->getProperty('decorated');
          $decorated_ref_prop->setAccessible(TRUE);
          $discovery = $decorated_ref_prop->getValue($parent_discovery);
          foreach ($early_decorators as $early_decorator_class) {
            $discovery = new $early_decorator_class($discovery);
          }
          $discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
        }
        $discovery = $discovery ?? $parent_discovery;
        foreach ($decorator->getMigrationDiscoveryDecorators() as $migration_discovery_decorator_class) {
          $discovery = new $migration_discovery_decorator_class($discovery);
        }
      }
      $this->discovery = $discovery ?? $parent_discovery;
    }
    return $this->discovery;
  }

  /**
   * Sets the migration discovery decorator manager.
   *
   * @param \Drupal\migration_decorator\MigrationDiscoveryDecoratorManagerInterface $manager
   *   The migration discovery decorator manager.
   */
  public function setDecoratorManager(MigrationDiscoveryDecoratorManagerInterface $manager): void {
    $this->decoratorManager = $manager;
  }

}
