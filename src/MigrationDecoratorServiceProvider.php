<?php

namespace Drupal\migration_decorator;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\migrate\Plugin\MigrationPluginManager as BaseMigrationPluginManager;
use Drupal\migrate_drupal\MigrationPluginManager as BaseMigrateDrupalMigrationPluginManager;
use Drupal\migration_decorator\Plugin\DrupalMigrationPluginManager;
use Drupal\migration_decorator\Plugin\MigrationPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Replaces migration plugin manager with our own implementation.
 */
class MigrationDecoratorServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if (!$container->hasDefinition('plugin.manager.migration')) {
      return;
    }
    $mpm_definition = $container->getDefinition('plugin.manager.migration');
    $original_class = $mpm_definition->getClass();

    if ($mpm_definition->getClass() === BaseMigrateDrupalMigrationPluginManager::class) {
      $mpm_definition->setClass(DrupalMigrationPluginManager::class);
    }
    if ($mpm_definition->getClass() === BaseMigrationPluginManager::class) {
      $mpm_definition->setClass(MigrationPluginManager::class);
    }

    if ($mpm_definition->getClass() === $original_class) {
      return;
    }

    $mpm_definition->addMethodCall(
      'setDecoratorManager',
      [new Reference('plugin.manager.migration_discovery_decorator', ContainerInterface::NULL_ON_INVALID_REFERENCE)]
    );
  }

}
