<?php

namespace Drupal\migration_decorator;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for migration discovery decorator plugins.
 */
interface MigrationDiscoveryDecoratorInterface extends PluginInspectionInterface {

  /**
   * FQCNs of decorators which should be called before derivative decorator.
   *
   * @return string[]
   *   FQCNs of early migration discovery decorators.
   */
  public function getEarlyMigrationDiscoveryDecorators(): array;

  /**
   * Returns FQCNs of migration discovery decorators.
   *
   * @return string[]
   *   FQCNs of migration discovery decorators.
   */
  public function getMigrationDiscoveryDecorators(): array;

  /**
   * Returns the weight of the plugin.
   *
   * @returns string[]
   */
  public function getWeight(): int;

}
